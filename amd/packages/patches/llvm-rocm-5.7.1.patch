Fix libpath to ensure standard path is not modified or overwritten.
NOTE: upstream clang-*-libc-search-path.patch does NOT apply cleanly
for ROCm versions based on LLVM 17. Therefore it is duplicated here.
diff --git a/clang/lib/Driver/Distro.cpp b/clang/lib/Driver/Distro.cpp
index 85d82180a805..b7bc4a28f5ab 100644
--- a/clang/lib/Driver/Distro.cpp
+++ b/clang/lib/Driver/Distro.cpp
@@ -98,6 +98,10 @@ static Distro::DistroType DetectLsbRelease(llvm::vfs::FileSystem &VFS) {
 }
 
 static Distro::DistroType DetectDistro(llvm::vfs::FileSystem &VFS) {
+  // The compiler should always behave the same, even when used via Guix on a
+  // foreign distro.
+  return Distro::UnknownDistro;
+
   Distro::DistroType Version = Distro::UnknownDistro;
 
   // Newer freedesktop.org's compilant systemd-based systems
diff --git a/clang/lib/Driver/ToolChains/AMDGPU.cpp b/clang/lib/Driver/ToolChains/AMDGPU.cpp
index fe3664647560..bab3b75b6314 100644
--- a/clang/lib/Driver/ToolChains/AMDGPU.cpp
+++ b/clang/lib/Driver/ToolChains/AMDGPU.cpp
@@ -271,6 +271,7 @@ RocmInstallationDetector::getInstallationPathCandidates() {
   ROCmSearchDirs.emplace_back(D.ResourceDir,
                               /*StrictChecking=*/true);
 
+  #if 0
   ROCmSearchDirs.emplace_back(D.SysRoot + "/opt/rocm",
                               /*StrictChecking=*/true);
 
@@ -318,6 +319,7 @@ RocmInstallationDetector::getInstallationPathCandidates() {
                                 /*StrictChecking=*/true);
   }
 
+  #endif
   DoPrintROCmSearchDirs();
   return ROCmSearchDirs;
 }
diff --git a/clang/lib/Driver/ToolChains/AMDGPUOpenMP.cpp b/clang/lib/Driver/ToolChains/AMDGPUOpenMP.cpp
index feaf0570b56f..c3e4df02ee94 100644
--- a/clang/lib/Driver/ToolChains/AMDGPUOpenMP.cpp
+++ b/clang/lib/Driver/ToolChains/AMDGPUOpenMP.cpp
@@ -214,9 +214,9 @@ const char *AMDGCN::OpenMPLinker::constructLLVMLinkCommand(
   }
 
   // If device debugging turned on, add specially built bc files
-  StringRef libpath = Args.MakeArgString(C.getDriver().Dir + "/../" + LibSuffix);
+  StringRef libpath = Args.MakeArgString(C.getDriver().InstalledDir + "/../" + LibSuffix);
   std::string lib_debug_perf_path = FindDebugPerfInLibraryPath(LibSuffix);
-  if (!lib_debug_perf_path.empty())
+  if (!lib_debug_perf_path.empty() && LibSuffix != "lib")
     libpath = lib_debug_perf_path;
 
   llvm::SmallVector<std::string, 12> BCLibs;
diff --git a/clang/lib/Driver/ToolChains/Cuda.cpp b/clang/lib/Driver/ToolChains/Cuda.cpp
index 7bc0892b6734..25de67d680b4 100644
--- a/clang/lib/Driver/ToolChains/Cuda.cpp
+++ b/clang/lib/Driver/ToolChains/Cuda.cpp
@@ -125,6 +125,9 @@ CudaInstallationDetector::CudaInstallationDetector(
     const Driver &D, const llvm::Triple &HostTriple,
     const llvm::opt::ArgList &Args)
     : D(D) {
+  // Don't look for CUDA in /usr.
+  return;
+
   struct Candidate {
     std::string Path;
     bool StrictChecking;
diff --git a/clang/lib/Driver/ToolChains/Linux.cpp b/clang/lib/Driver/ToolChains/Linux.cpp
index 94c26cb94ee9..56c23ed2e24c 100644
--- a/clang/lib/Driver/ToolChains/Linux.cpp
+++ b/clang/lib/Driver/ToolChains/Linux.cpp
@@ -187,6 +187,10 @@ Linux::Linux(const Driver &D, const llvm::Triple &Triple, const ArgList &Args)
 
   Generic_GCC::PushPPaths(PPaths);
 
+  // Comment out the distro-specific tweaks so that they don't bite when
+  // using Guix on a foreign distro.
+#if 0
+
   Distro Distro(D.getVFS(), Triple);
 
   if (Distro.IsAlpineLinux() || Triple.isAndroid()) {
@@ -247,6 +251,7 @@ Linux::Linux(const Driver &D, const llvm::Triple &Triple, const ArgList &Args)
 
 #ifdef ENABLE_LINKER_BUILD_ID
   ExtraOpts.push_back("--build-id");
+#endif
 #endif
 
   // The selection of paths to try here is designed to match the patterns which
@@ -268,6 +273,7 @@ Linux::Linux(const Driver &D, const llvm::Triple &Triple, const ArgList &Args)
   }
   Generic_GCC::AddMultilibPaths(D, SysRoot, OSLibDir, MultiarchTriple, Paths);
 
+#if 0
   addPathIfExists(D, concat(SysRoot, "/lib", MultiarchTriple), Paths);
   addPathIfExists(D, concat(SysRoot, "/lib/..", OSLibDir), Paths);
 
@@ -296,9 +302,11 @@ Linux::Linux(const Driver &D, const llvm::Triple &Triple, const ArgList &Args)
     addPathIfExists(D, concat(SysRoot, "/", OSLibDir, ABIName), Paths);
     addPathIfExists(D, concat(SysRoot, "/usr", OSLibDir, ABIName), Paths);
   }
+#endif
 
   Generic_GCC::AddMultiarchPaths(D, SysRoot, OSLibDir, Paths);
 
+#if 0
   // The deprecated -DLLVM_ENABLE_PROJECTS=libcxx configuration installs
   // libc++.so in D.Dir+"/../lib/". Detect this path.
   // TODO Remove once LLVM_ENABLE_PROJECTS=libcxx is unsupported.
@@ -308,6 +316,14 @@ Linux::Linux(const Driver &D, const llvm::Triple &Triple, const ArgList &Args)
 
   addPathIfExists(D, concat(SysRoot, "/lib"), Paths);
   addPathIfExists(D, concat(SysRoot, "/usr/lib"), Paths);
+#endif
+
+  // Add libc's lib/ directory to the search path, so that crt1.o, crti.o,
+  // and friends can be found.
+  addPathIfExists(D, "@GLIBC_LIBDIR@", Paths);
+
+  // Add GCC's lib/ directory so libstdc++.so can be found.
+  addPathIfExists(D, GCCInstallation.getParentLibPath(), Paths);
 }
 
 ToolChain::RuntimeLibType Linux::GetDefaultRuntimeLibType() const {
